# crawler

Promise based link-crawler with default logger.

**Set the params where**:

-l - is a link for crawl

-c - is a count of stored link for each page

-d - is a depth of crawl

**example**

node crawl.js -l https://ru.wikipedia.org -c 20 -d 2

