const Crawling = require('./classes/Crawling')
const yargs = require('yargs/yargs')
const { hideBin } = require('yargs/helpers')
const argv = yargs(hideBin(process.argv)).argv
console.clear()
console.info(`link: ${argv.l}`)
console.info(`depth: ${argv.d}`)
console.info(`count: ${argv.c}`)
console.info("Processing...")

const crawler = new Crawling(argv.l, argv.d, argv.c)

let links = [];

const promises = [];

let resultObtained = false


function makeRequest(link, step){
  processRequest(crawler.collector(link, step))
}

function processRequest(requestPromise){
  promises.push(requestPromise)
  requestPromise.then(dispatchResult).catch(error => {
    if (error) {
      console.error(error)
      process.exit(1)
    }
  })
}

function dispatchResult(result) {
  if (result.step > argv.d){
    if (!resultObtained) {
      processResult()
    }
  }else{
    links = links.concat(result.links)
    renewAction(result.links, result.step +1)
  }
}

function renewAction(links, step){
  links.map(link => {
    makeRequest(link, step)
  })
}

function processResult(){
  resultObtained = true
  Promise.all(promises).then(() => {
    console.clear()
    console.log(links)
    console.log(`total items: ${links.length}`)
    process.exit(0)
  })
}

function mainCycle(){
  makeRequest('', 1)
}
mainCycle()
