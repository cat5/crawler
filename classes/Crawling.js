const rp = require('request-promise');
const cheerio = require('cheerio');
const winston = require('winston');

const logger = winston.createLogger({
  level: 'info',
  format: winston.format.json(),
  defaultMeta: { service: 'crawler' },
  transports: [
    new winston.transports.File({ filename: 'error.log', level: 'error' }),
  ],
});

module.exports = class Crawling{
  baseLink = ''
  depth = 1
  amt = 1

  constructor(baseLink, depth, amt) {
    this.baseLink = baseLink
    this.depth = depth
    this.amt = amt
  }

  collector = (link = '', step) => {
    return rp(this.defineLink(link))
      .then((html) =>{
        let $ = cheerio.load(html)
        let links = []
        $('a').each((index, el) => {
          if (links.length < this.amt && el.attribs.href) {
            links.push(el.attribs.href)
          }
        })
        return {
          links: links,
          step: step
        }
      }).catch(function(err){
        logger.error(`got error on request: ${err.statusCode} ${err.options.uri}`)
        return {
          links: [],
          step: step
        }
    });
  }

  defineLink = (link) => {
    let actionLink = `${this.baseLink}${link}`
    if (link.includes('https://')){
      actionLink = link;
    }
    return actionLink;
  }
}
